//Trabajo de Bocelo y Tapia

import java.util.Date;

public class Tarjeta {
    private int numero;
    private Date fechaVencimiento;
    private String nombrePropietario;

    public Tarjeta(int numero, Date fechaVencimiento, String nombrePropietario) {
        this.numero = numero;
        this.fechaVencimiento = fechaVencimiento;
        this.nombrePropietario = nombrePropietario;
    }

    public int getNumero() {
        return numero;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public String getNombrePropietario() {
        return nombrePropietario;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setNombrePropietario(String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }
}
