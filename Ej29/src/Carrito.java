//Trabajo de Bocelo y Tapia

import java.util.HashMap;

public class Carrito {
    private HashMap<Producto, Integer> productos;
    private float subtotal;

    public Carrito(HashMap<Producto, Integer> productos) {
        this.productos = productos;
        subtotal = calcularSubtotal();
    }

    public HashMap<Producto, Integer> getProductos() {
        return productos;
    }

    public void setProductos(HashMap<Producto, Integer> productos) {
        this.productos = productos;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    private float calcularSubtotal(){
        float subtotal = 0;

        for(Producto producto : productos.keySet()){
            float cantidad = productos.get(producto);
            subtotal = subtotal + (producto.getPrecio() * cantidad);
        }

        return subtotal;
    }

    public void AgregarProducto(Producto producto, int cantidad){
        productos.put(producto, cantidad);
        subtotal = calcularSubtotal();
    }

    public void EliminarProducto(Producto producto){
        for(Producto productoMap : productos.keySet()){
            if(producto == productoMap){
                productos.remove(producto);
            }
        }
        subtotal = calcularSubtotal();
    }

    public void ModificarCantidad(Producto producto, int cantidad){
        for(Producto productoMap : productos.keySet()){
            if(producto == productoMap){
                productos.replace(producto, cantidad);
            }
        }
        subtotal = calcularSubtotal();
    }
}
