//Trabajo de Bocelo y Tapia

public class Compra {
    private Carrito carrito;
    private Cliente cliente;
    private Tarjeta tarjeta;
    private float precioEnvio;
    private float precioTotal;

    public Compra(Carrito carrito, Cliente cliente, Tarjeta tarjeta, float precioEnvio) {
        this.carrito = carrito;
        this.cliente = cliente;
        this.tarjeta = tarjeta;
        this.precioEnvio = precioEnvio;
        precioTotal = CalcularPrecioTotal();
    }

    public Carrito getCarrito() {
        return carrito;
    }

    public void setCarrito(Carrito carrito) {
        this.carrito = carrito;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public float getPrecioEnvio() {
        return precioEnvio;
    }

    public void setPrecioEnvio(float precioEnvio) {
        this.precioEnvio = precioEnvio;
    }

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    private float CalcularPrecioTotal(){
        float precioTotal = carrito.getSubtotal() + precioEnvio;
        return precioTotal;
    }
}
