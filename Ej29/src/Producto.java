//Trabajo de Bocelo y Tapia

import java.util.ArrayList;

public class Producto {
    private String nombre;
    private float precio;
    private Vendedor vendedor;
    private String descripcion;
    private ArrayList<String> fotos;
    private int stock;
    private String categoria;

    public Producto(String nombre, float precio, Vendedor vendedor, String descripcion, ArrayList<String> fotos, int stock, String categoria) {
        this.nombre = nombre;
        this.precio = precio;
        this.vendedor = vendedor;
        this.descripcion = descripcion;
        this.fotos = fotos;
        this.stock = stock;
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<String> getFotos() {
        return fotos;
    }

    public void setFotos(ArrayList<String> fotos) {
        this.fotos = fotos;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
