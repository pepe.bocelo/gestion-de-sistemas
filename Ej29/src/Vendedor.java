//Trabajo de Bocelo y Tapia

public class Vendedor {
    private String nombre;
    private String direccion;
    private int cbu;
    private int reputacion;

    public Vendedor(String nombre, String direccion, int cbu) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.cbu = cbu;
        reputacion = 0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCbu() {
        return cbu;
    }

    public void setCbu(int cbu) {
        this.cbu = cbu;
    }

    public int getReputacion() {
        return reputacion;
    }

    public void setReputacion(int reputacion) {
        this.reputacion = reputacion;
    }
}
