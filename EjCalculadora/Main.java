import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Operaciones operacion = new Operaciones(0,0,"");

        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);

        System.out.println("Ingrese un numero");
        int numero1 = sc.nextInt();
        System.out.println("Ingrese un operador");
        String operator = sc2.nextLine();
        System.out.println("Ingrese otro numero");
        int numero2 = sc.nextInt();

        operacion.setOperador(operator);
        operacion.setNumero1(numero1);
        operacion.setNumero2(numero2);

        System.out.println(operacion.realizarOperacion(operacion));

    }
}