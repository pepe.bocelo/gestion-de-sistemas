public class Operaciones {
    private int numero1;
    private int numero2;
    private String operador;

    public Operaciones(int numero1, int numero2, String operador) {
        this.numero1 = numero1;
        this.numero2 = numero2;
        this.operador = operador;
    }

    public void verificarDivisionX0(Operaciones operacion){
        if(operacion.numero2 == 0 && operador.equals("/")){
            System.out.println("Error 1");
        }
    }

    public void verificarOperador(Operaciones operacion){
        if(!operacion.operador.equals("/") || !operacion.operador.equals("-") || !operacion.operador.equals("+") || !operacion.operador.equals("*")){
            System.out.println("Error 2");
        }
    }

    public float realizarOperacion(Operaciones operacion){
        if(operacion.operador.equals("*")){
            return (operacion.numero1*operacion.numero2);
        } else if(operacion.operador.equals("/")){
            return (operacion.numero1/operacion.numero2);
        }
        else if (operacion.operador.equals("+")){
            return (operacion.numero1+operacion.numero2);
        } else return (operacion.numero1-operacion.numero2);
    }

    public int getNumero2() {
        return numero2;
    }

    public void setNumero2(int numero2) {
        this.numero2 = numero2;
    }

    public void setNumero1(int numero1) {
        this.numero1 = numero1;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }
}
