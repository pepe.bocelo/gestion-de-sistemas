import java.util.ArrayList;

public class Impresion {
    private int hojasRequeridas;
    private float tintaRequerida;
    private Impresora impresora;
    private Prioridad prioridad;

    public Impresion(int hojasRequeridas, float tintaRequerida, Impresora impresora) {
        this.hojasRequeridas = hojasRequeridas;
        this.tintaRequerida = tintaRequerida;
        this.impresora = impresora;
        prioridad = impresora.getPrioridad();
    }

    public int getHojasRequeridas() {
        return hojasRequeridas;
    }

    public void setHojasRequeridas(int hojasRequeridas) {
        this.hojasRequeridas = hojasRequeridas;
    }

    public float getTintaRequerida() {
        return tintaRequerida;
    }

    public void setTintaRequerida(float tintaRequerida) {
        this.tintaRequerida = tintaRequerida;
    }

    public Impresora getImpresora() {
        return impresora;
    }

    public void setImpresora(Impresora impresora) {
        this.impresora = impresora;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    public ArrayList<Impresion> priorizar(){
        ArrayList<Impresion> cola = prioridad.priorizar(impresora.getColaImpresion());
        return cola;
    }
}
