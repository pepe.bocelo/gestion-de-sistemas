import java.util.ArrayList;

public abstract class Prioridad {
    public abstract ArrayList<Impresion> priorizar(ArrayList<Impresion> colaImpresion);
}
