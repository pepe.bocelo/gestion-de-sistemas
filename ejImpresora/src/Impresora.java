import java.util.ArrayList;

public class Impresora {
    private String nombre;
    private ArrayList<Dispositivo> dispositivos;
    private ArrayList<Impresion> colaImpresion;
    private int hojas;
    private float tinta;
    private Prioridad prioridad;
    private Impresora impresora;

    public Impresora newImpresora(String nombre, ArrayList<Dispositivo> dispositivos, ArrayList<Impresion> colaImpresion, int hojas, float tinta, Prioridad prioridad){
        if (impresora == null){
            impresora = new Impresora(nombre, dispositivos, colaImpresion, hojas, tinta, prioridad);
        }
        else{
            System.out.println("No se puede crear otra impresora");
        }

        return impresora;
    }

    private Impresora(String nombre, ArrayList<Dispositivo> dispositivos, ArrayList<Impresion> colaImpresion, int hojas, float tinta, Prioridad prioridad) {
        this.nombre = nombre;
        this.dispositivos = dispositivos;
        this.colaImpresion = colaImpresion;
        this.hojas = hojas;
        this.tinta = tinta;
        this.prioridad = prioridad;
    }

    public Impresora(Impresora impresora) {
        this.impresora = impresora;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public void setDispositivos(ArrayList<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
    }

    public ArrayList<Impresion> getColaImpresion() {
        return colaImpresion;
    }

    public void setColaImpresion(ArrayList<Impresion> colaImpresion) {
        this.colaImpresion = colaImpresion;
    }

    public int getHojas() {
        return hojas;
    }

    public void setHojas(int hojas) {
        this.hojas = hojas;
    }

    public float getTinta() {
        return tinta;
    }

    public void setTinta(float tinta) {
        this.tinta = tinta;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    public Impresora getImpresora() {
        return impresora;
    }

    public void setImpresora(Impresora impresora) {
        this.impresora = impresora;
    }

    public void agregarImpresionACola(Impresion impresion){
        colaImpresion.add(impresion);
    }
}
