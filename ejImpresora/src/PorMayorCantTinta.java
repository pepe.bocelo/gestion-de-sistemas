import java.util.ArrayList;

public class PorMayorCantTinta {
    public ArrayList<Impresion> priorizar(ArrayList<Impresion> colaImpresion){
        ArrayList<Impresion> colaOrdenada = new ArrayList<>();

        for (Impresion impresion : colaImpresion) {

            if(colaOrdenada.isEmpty()){
                colaOrdenada.add(impresion);
            }

            else {
                boolean añadida = comprobarSiEsMayor(colaOrdenada, impresion);

                if (añadida == false){
                    colaOrdenada.add(impresion);
                }
            }
        }

        return colaOrdenada;
    }

    private boolean comprobarSiEsMayor(ArrayList<Impresion> colaOrdenada, Impresion impresion){
        boolean añadida = true;
        int posicion = 0;

        for (Impresion impresionAComparar : colaOrdenada) {
            if (impresion.getTintaRequerida() > impresionAComparar.getTintaRequerida()) {
                colaOrdenada.add(posicion, impresion);
                añadida = true;
            }
            posicion ++;
        }

        return añadida;
    }
}
